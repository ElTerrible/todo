import React, {ChangeEvent} from "react";
import {FilterValuesType} from "./App";
import AddItemForm from "./AddItemForm";
import EditableSpan from "./EditableSpan";
import { pink } from '@mui/material/colors';
import {IconButton, Button, Checkbox} from "@mui/material";
import {Delete} from "@mui/icons-material";

export type TaskType = {
    id: string,
    title: string,
    isDone: boolean,
}
type PropsType = {
    id: string,
    title: string,
    tasks: Array<TaskType>,
    removeTask: (id: string, todoListId: string) => void,
    changeFilter: (value: FilterValuesType, todoListId: string) => void,
    addTask: (title: string, todoListId: string) => void,
    changeTaskStatus: (taskId: string, isDone: boolean, todoListId: string) => void,
    changeTaskTitle: (id: string, newTitle: string, todoListId: string) => void,
    filter: FilterValuesType,
    removeTodolist: (todoListId: string) => void,
    changeTodoListTitle: (id:string, newTitle: string) => void,
}

export function TodoList(props: PropsType) {

    const removeTodolistClick = () => {
        props.removeTodolist(props.id)
    }

    const changeTodoListTitle = (newTitle:string) => {
        props.changeTodoListTitle(props.id,newTitle)
    }

    const onAllClickHandler = () => props.changeFilter('all', props.id);
    const onCompletedClickHandler = () => props.changeFilter('completed', props.id);
    const onActiveClickHandler = () =>props.changeFilter('active', props.id);

    const addTask = (title: string) => {
        props.addTask(title, props.id)
    }

    return (
        <div>
            <h3>
                <EditableSpan title={props.title} onChange={changeTodoListTitle} />
                <IconButton onClick={removeTodolistClick}>
                    <Delete></Delete>
                </IconButton>
            </h3>
            <AddItemForm addItem={addTask} />
            <div>
                {
                    props.tasks.map((task, index) => {
                        const onRemoveHandler = () => {
                            props.removeTask(task.id, props.id)
                        }
                        const onChangeStatusHandler = (e: ChangeEvent<HTMLInputElement>) => {
                            props.changeTaskStatus(task.id, e.currentTarget.checked, props.id)
                        }
                        const onChangeTitleHandler = (newValue: string) => {
                            props.changeTaskTitle(task.id, newValue, props.id)
                        }
                        return (
                            <div key={index} className={task.isDone ? "is-done" : ""}>
                                <Checkbox
                                    sx={{
                                        color: pink[800],
                                        '&.Mui-checked': {
                                            color: pink[600],
                                        },
                                    }}
                                    checked={task.isDone}
                                    onChange={onChangeStatusHandler}
                                />
                                <EditableSpan title={task.title} onChange={onChangeTitleHandler}/>
                                <IconButton onClick={onRemoveHandler} size={'small'}>
                                    <Delete fontSize="small"/>
                                </IconButton>
                            </div>
                        )
                    })
                }
            </div>
            <div>
                <Button
                    variant={props.filter === 'all' ? 'contained' : 'text'}
                    onClick={onAllClickHandler}
                >All</Button>
                <Button
                    variant={props.filter === 'active' ? 'contained' : 'text'}
                    onClick={onActiveClickHandler}
                >Active</Button>
                <Button
                    variant={props.filter === 'completed' ? 'contained' : 'text'}
                    onClick={onCompletedClickHandler}
                >Completed</Button>
            </div>
        </div>
    )
}
