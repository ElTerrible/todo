import {TasksTypeState, TodolistType} from "../App";
import {AddTodoListAC, todoListsReducer} from "./todoLists-reducer";
import {tasksReducer} from "./tasks-reducer";

test('ids should be equals', () => {
    const startTasksState: TasksTypeState = {};
    const startTodoListsState: Array<TodolistType> = [];

    const action = AddTodoListAC("new todoLists");

    const endTasksState = tasksReducer(startTasksState, action);
    const endTodoListsState = todoListsReducer(startTodoListsState, action);

    const keys = Object.keys(endTasksState);
    const idFromTasks = keys[0];
    const idFromTodoLists = endTodoListsState[0].id;

    expect(idFromTasks).toBe(action.todoListId);
    expect(idFromTodoLists).toBe(action.todoListId);
})