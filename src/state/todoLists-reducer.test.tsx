import {
    AddTodoListAC, ChangeTodoListFilterAC,
    ChangeTodoListTitleAC,
    RemoveTodoListAC,
    todoListsReducer
} from "./todoLists-reducer";
import {v1} from "uuid";
import {FilterValuesType, TodolistType} from "../App";

test('correct todoList should be removed', () => {
    let todoListId1 = v1();
    let todoListId2 = v1();

    const startState: Array<TodolistType> = [
        {id: todoListId1, title: 'What to learn', filter: 'all'},
        {id: todoListId2, title: 'What to buy', filter: 'all'},
    ];

    const endState = todoListsReducer(startState, RemoveTodoListAC(todoListId1));

    expect(endState.length).toBe(1);
    expect(endState[0].id).toBe(todoListId2);
})

test('correct todoList should be added', () => {
    let todoListId1 = v1();
    let todoListId2 = v1();

    let newTodoListTitle = 'New Todolist';

    const startState: Array<TodolistType> = [
        {id: todoListId1, title: 'What to learn', filter: 'all'},
        {id: todoListId2, title: 'What to buy', filter: 'all'},
    ];

    const endState = todoListsReducer(startState, AddTodoListAC(newTodoListTitle));

    expect(endState.length).toBe(3);
    expect(endState[2].title).toBe(newTodoListTitle);
    expect(endState[2].filter).toBe('all');
})

test('correct todoList should change its name', () => {
    let todoListId1 = v1();
    let todoListId2 = v1();

    let newTodoListTitle = 'New Todolist';

    const startState: Array<TodolistType> = [
        {id: todoListId1, title: 'What to learn', filter: 'all'},
        {id: todoListId2, title: 'What to buy', filter: 'all'},
    ];

    const action = ChangeTodoListTitleAC(todoListId2,newTodoListTitle);
    // const action = {
    //     type: 'CHANGE-TODOLIST-TITLE' as const,
    //     id: todoListId2,
    //     title: newTodoListTitle,
    // }

    const endState = todoListsReducer(startState, action);

    expect(endState[0].title).toBe("What to learn");
    expect(endState[1].title).toBe(newTodoListTitle);
})

test('correct todoList should change its filter', () => {
    let todoListId1 = v1();
    let todoListId2 = v1();

    let newFilter: FilterValuesType = 'completed';

    const startState: Array<TodolistType> = [
        {id: todoListId1, title: 'What to learn', filter: 'all'},
        {id: todoListId2, title: 'What to buy', filter: 'all'},
    ];

    // const action: ChangeTodoListFilterActionType = {
    //     type: 'CHANGE-TODOLIST-FILTER',
    //     id: todoListId2,
    //     filter: newFilter,
    // }

    const endState = todoListsReducer(startState,ChangeTodoListFilterAC(todoListId2, newFilter));

    expect(endState[0].filter).toBe('all');
    expect(endState[1].filter).toBe('completed');
})