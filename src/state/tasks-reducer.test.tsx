import {addTaskAC, changeTaskStatusAC, changeTaskTitleAC, removeTaskAC, tasksReducer} from "./tasks-reducer";
import {TasksTypeState} from "../App";
import {AddTodoListAC, RemoveTodoListAC} from "./todoLists-reducer";


test('correct task should be deleted from correct array', () => {
    const startState: TasksTypeState = {
        "todoListId1" : [
            {id: "1", title: 'CSS', isDone: true},
            {id: "2", title: 'JS', isDone: true},
            {id: "3", title: 'React', isDone: false},
            {id: "4", title: 'Redux', isDone: false},
            {id: "5", title: 'SQL', isDone: false},
        ],
        "todoListId2": [
            {id: "1", title: 'Book', isDone: false},
            {id: "2", title: 'tea', isDone: true},
        ]
    }

    const action = removeTaskAC("2", "todoListId2");
    const endState = tasksReducer(startState,action);

    expect(endState["todoListId1"].length).toBe(5);
    expect(endState["todoListId2"].length).toBe(1);
    expect(endState["todoListId2"].every(t => t.id != "2")).toBeTruthy();
})

test('correct task should be added from correct array', () => {
    const startState: TasksTypeState = {
        "todoListId1" : [
            {id: "1", title: 'CSS', isDone: true},
            {id: "2", title: 'JS', isDone: true},
            {id: "3", title: 'React', isDone: false},
            {id: "4", title: 'Redux', isDone: false},
            {id: "5", title: 'SQL', isDone: false},
        ],
        "todoListId2": [
            {id: "1", title: 'Book', isDone: false},
            {id: "2", title: 'tea', isDone: true},
        ]
    }

    const action = addTaskAC("juice","todoListId2");
    const endState = tasksReducer(startState,action);

    expect(endState["todoListId1"].length).toBe(5);
})

test('status of specified task should be changed', () => {
    const startState: TasksTypeState = {
        "todoListId1" : [
            {id: "1", title: 'CSS', isDone: true},
            {id: "2", title: 'JS', isDone: true},
            {id: "3", title: 'React', isDone: false},
            {id: "4", title: 'Redux', isDone: false},
            {id: "5", title: 'SQL', isDone: false},
        ],
        "todoListId2": [
            {id: "1", title: 'Book', isDone: false},
            {id: "2", title: 'tea', isDone: true},
        ]
    }

    const action = changeTaskStatusAC("2", false, "todoListId2");
    const endState = tasksReducer(startState,action);

    expect(endState["todoListId2"][1].isDone).toBeFalsy();
    expect(endState["todoListId1"][1].isDone).toBeTruthy();
})

test('title of specified task should be changed', () => {
    const startState: TasksTypeState = {
        "todoListId1" : [
            {id: "1", title: 'CSS', isDone: true},
            {id: "2", title: 'JS', isDone: true},
            {id: "3", title: 'React', isDone: false},
            {id: "4", title: 'Redux', isDone: false},
            {id: "5", title: 'SQL', isDone: false},
        ],
        "todoListId2": [
            {id: "1", title: 'Book', isDone: false},
            {id: "2", title: 'tea', isDone: true},
        ]
    }

    const action = changeTaskTitleAC("2", "Snickers", "todoListId2");
    const endState = tasksReducer(startState,action);

    expect(endState["todoListId2"][1].title).toBe("Snickers");
    expect(endState["todoListId1"][1].title).toBe("JS");
})

test('new property with array should be added when new todoList is added', () => {
    const startState: TasksTypeState = {
        "todoListId1" : [
            {id: "1", title: 'CSS', isDone: true},
            {id: "2", title: 'JS', isDone: true},
            {id: "3", title: 'React', isDone: false},
            {id: "4", title: 'Redux', isDone: false},
            {id: "5", title: 'SQL', isDone: false},
        ],
        "todoListId2": [
            {id: "1", title: 'Book', isDone: false},
            {id: "2", title: 'tea', isDone: true},
        ]
    }

    const action = AddTodoListAC("title no matter");
    const endState = tasksReducer(startState,action);

    const keys = Object.keys(endState);
    const newKey = keys.find(k => k != "todoListId1" && k != "todoListId2");
    if (!newKey) {
        throw new Error("new key should be added")
    }
    expect(keys.length).toBe(3);
    expect(endState[newKey]).toStrictEqual([])
})

test('property with todoListId should be deleted', () => {
    const startState: TasksTypeState = {
        "todoListId1" : [
            {id: "1", title: 'CSS', isDone: true},
            {id: "2", title: 'JS', isDone: true},
            {id: "3", title: 'React', isDone: false},
            {id: "4", title: 'Redux', isDone: false},
            {id: "5", title: 'SQL', isDone: false},
        ],
        "todoListId2": [
            {id: "1", title: 'Book', isDone: false},
            {id: "2", title: 'tea', isDone: true},
        ]
    }

    const action = RemoveTodoListAC("todoListId2");
    const endState = tasksReducer(startState, action);

    const keys = Object.keys(endState);

    expect(keys.length).toBe(1);
    expect(endState["todoListId2"]).toBeUndefined();
})
