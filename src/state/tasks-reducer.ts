import {TasksTypeState} from "../App";
import {v1} from "uuid";
import {AddTodoListActionType, RemoveTodoListActionType} from "./todoLists-reducer";


export type RemoveTaskActionType = {
    type: 'REMOVE-TASK',
    todoListId: string,
    taskId: string,
}

export type AddTaskActionType = {
    type: 'ADD-TASK',
    title: string,
    todoListId: string,
}

export type ChangeTaskStatusType = {
    type: 'CHANGE-TASK-STATUS',
    taskId: string,
    isDone: boolean,
    todoListId: string,
}

export type ChangeTaskTitleType = {
    type: 'CHANGE-TASK-TITLE',
    taskId: string,
    title: string,
    todoListId: string,
}

type ActionsType =
    RemoveTaskActionType
    | AddTaskActionType
    | ChangeTaskStatusType
    | ChangeTaskTitleType
    | AddTodoListActionType
    | RemoveTodoListActionType;

export const tasksReducer = (state: TasksTypeState, action: ActionsType) => {
    switch (action.type) {
        case 'REMOVE-TASK': {
            const stateCopy = {...state};
            const tasks = state[action.todoListId];
            const filteredTasks = tasks.filter(t => t.id !== action.taskId);
            stateCopy[action.todoListId] = filteredTasks;
            return stateCopy;
        }
        case 'ADD-TASK': {
            const stateCopy = {...state};
            const tasks = stateCopy[action.todoListId];
            const newTask = {id: v1(), title: action.title, isDone: false}
            const newTasks = [newTask, ...tasks]
            stateCopy[action.todoListId] = newTasks;
            return stateCopy
        }
        case "CHANGE-TASK-STATUS": {
            const stateCopy = {...state};

            let tasks = stateCopy[action.todoListId]
            let task = tasks.find(t => t.id === action.taskId);
            if (task) {
                task.isDone = action.isDone;
            }

            return stateCopy;
        }
        case "CHANGE-TASK-TITLE": {
            const stateCopy = {...state};

            let tasks = stateCopy[action.todoListId]
            let task = tasks.find(t => t.id === action.taskId);
            if (task) {
                task.title = action.title;
            }

            return stateCopy;
        }
        case 'ADD-TODOLIST': {
            const stateCopy = {...state};
            stateCopy[action.todoListId] = [];
            return stateCopy;
        }
        case "REMOVE-TODOLIST": {
            const stateCopy = {...state};
            delete stateCopy[action.id];
            return stateCopy;
        }
    }
}

export const removeTaskAC = (taskId: string, todoListId: string): RemoveTaskActionType => {
    return {type: 'REMOVE-TASK', taskId: taskId, todoListId: todoListId}
}

export const addTaskAC = (title: string, todoListId: string): AddTaskActionType => {
    return {type: 'ADD-TASK', title, todoListId}
}

export const changeTaskStatusAC = (taskId: string, isDone: boolean, todoListId: string): ChangeTaskStatusType => {
    return {type: 'CHANGE-TASK-STATUS', isDone, todoListId, taskId}
}

export const changeTaskTitleAC = (taskId: string, title: string, todoListId: string): ChangeTaskTitleType => {
    return {type: 'CHANGE-TASK-TITLE', title, todoListId, taskId}
}