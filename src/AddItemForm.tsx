import React, {ChangeEvent, KeyboardEvent} from "react";
import { TextField} from "@mui/material";
import IconButton from '@mui/material/IconButton';
import {ControlPoint} from "@mui/icons-material";

export type addItemFormPropsType = {
    addItem: (title: string) => void,
}

function AddItemForm(props:addItemFormPropsType) {

    const [title, setTitle] = React.useState<string>('');
    const [error, setError] = React.useState<string | null>(null);

    const addTask = () => {
        if (title.trim() !== '') {
            props.addItem(title);
            setTitle('')
        }else {
            setError('Title is required')
        }
    }

    const onNewTitleChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setTitle(e.currentTarget.value)
    }
    const onKeyPressHandler = (e:KeyboardEvent<HTMLInputElement>) => {
        setError(null)
        if (e.charCode === 13) {
            addTask();
        }
    }
    return <div>
        <TextField
            variant={'outlined'}
            label={'Type value'}
            className={error ? "error" : ""}
            value={title}
            onChange={onNewTitleChangeHandler}
            onKeyPress={onKeyPressHandler}
            error={!!error}
            helperText={error}
        />
        <IconButton onClick={addTask}  color={'primary'}>
            <ControlPoint />
        </IconButton>
    </div>
}

export default AddItemForm;